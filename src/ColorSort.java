public class ColorSort {

   enum Color {red, green, blue};

   public static void main (String[] param) {
      // for debugging
      Color[] balls = new ColorSort.Color [10];
      for (int i=0; i < balls.length; i++) {
         double rnd = Math.random();
         if (rnd < 1./3.) {
            balls[i] = ColorSort.Color.red;
         } else  if (rnd > 2./3.) {
            balls[i] = ColorSort.Color.blue;
         } else {
            balls[i] = ColorSort.Color.green;
         }
      }
      ColorSort.reorder (balls);
   }
   
   public static void reorder (Color[] balls) {
      int[] enumCounts = new int[Color.values().length];

      // this loop maps ball colors, ordinal marks the place in enum
      for (Color a : balls)
      {
         ++enumCounts[a.ordinal()];
      }

      // start replacing all items in balls array by manually overriding items
      int out = 0;
      for (int n = 0; n < enumCounts[0]; n++) {
         balls[out++] = Color.red;
      }
      for (int n = 0; n < enumCounts[1]; n++) {
         balls[out++] = Color.green;
      }
      for (int n = 0; n < enumCounts[2]; n++) {
         balls[out++] = Color.blue;
      }

   }

   public static void reorderByReplacing (Color[] balls) {
      Color color;
      for (int i=1; i < balls.length; i++)
      {
         for (int j = 0; j < i; j++) {
            // compare by ordinals
            if (balls[i].compareTo(balls[j]) < 0) {
               color = balls[i];
               balls[i] = balls[j];
               balls[j] = color;
            }

         }
      }
   }
}

